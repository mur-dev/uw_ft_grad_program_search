# UW FT Grad Program Search Module
This module allows a user to search for graduate programs.

[uwaterloo.ca/future-students/programs](https://uwaterloo.ca/future-students/programs)

[uwaterloo.ca/future-students/grad-programs/results](https://uwaterloo.ca/future-students/grad-programs/results)

---
---
# Functions

---
---
### function uw_ft_grad_program_search_menu()
This hook enables the module to register paths in order to define how URL requests are handled.

##### Returns
An associative array whose keys define paths and whose values are an associative array of properties for each path.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7.x) for this specific hook for more information.

---
---
### function uw_ft_grad_program_search_field_info()
Returns an array whose keys are field type names and whose values are arrays describing the field type, with the following key/value pairs:
- Label: The human-readable name of the field type.
- Description: A short description for the field type.
- Default_widget: The machine name of the default widget to be used by instances of this field type, when no widget is specified in the instance definition. This widget must be available whenever the field type is available (i.e. provided by the field type module, or by a module the field type module depends on).
- Default_formatter: The machine name of the default formatter to be used by instances of this field type, when no formatter is specified in the instance definition. This formatter must be available whenever the field type is available (i.e. provided by the field type module, or by a module the field type module depends on).

##### Returns
An array describing information.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_info/7.x) for this specific hook for more information.

---
---
function uw_ft_grad_program_search_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors)
Validate this module's field data. Allows us to validate the form settings, if needed. Drupal requires this function to exist for the module to work. Currently, it is empty.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_validate/7.x) for this specific hook for more information.

---
---
### function uw_ft_grad_program_search_field_is_empty($item, $field)
Defines what constitutes an empty item for a field type.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules!field!field.api.php/function/hook_field_is_empty/7.x) for this specific hook for more information.

---
---
### function uw_ft_grad_program_search_field_formatter_info()
Expose Field API formatter types. Declares that we will be using a custom view for the field, as a form.

##### Returns
An array describing the formatter types implemented by the module. The keys are formatter type names. To avoid name clashes, formatter type names should be prefixed with the name of the module that exposes them. The values are arrays describing the formatter type, with the following key/value pairs:
- Label: The human-readable name of the formatter type.
- Field types: An array of field types the formatter supports.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_formatter_info/7.x) for this specific hook for more information.

---
---
### function uw_ft_grad_program_search_results($form, $form_state)
Displays the results page where it shows which programs match with the keyword(s) they have entered.

##### Returns
The results page.

---
---
### function uw_ft_grad_program_search_display($form, $form_state, $item)
This is the initial landing page. This is where the user will enter their keyword(s) to search for programs.

##### Returns
The initial landing page.

function uw_ft_grad_program_search_display_submit($form, &$form_state)
This submits the form and takes the user to the results page.

---
---
### function uw_ft_grad_program_search_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
Build a renderable array for a field value. This calls uw_ft_grad_program_search form as the display for the form.

##### Returns
The initial landing page.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_formatter_view/7.x) for this specific hook for more information.

---
---
### function uw_ft_grad_program_search_field_widget_info()
Exposes Field API widget types. Declares uw_ft_grad_program_search_widget as a widget.

##### Returns
An array describing the widget types implemented by the module. The keys are widget type names. To avoid name clashes, widget type names should be prefixed with the name of the module that exposes them. The values are arrays describing the widget type, with the following key/value pairs:
- Label: The human-readable name of the widget type.
- Field types: An array of field types the widget supports.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_widget_info/7.x) for this specific hook for more information.

---
---
### function uw_ft_grad_program_search_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element)
Returns the form for a single field widget. This is where Drupal tells us to create form elements for our field's widget.

##### Returns
the form element.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_widget_form/7.x) for this specific hook for more information.

---
---
### function uw_ft_grad_program_search_widget_error($element, $error, $form, &$form_state)
Flags a field-level validation error. Lets us figure out what to do with errors we might have generated in hook_field_validate(). Generally, we'll just call form_error().

function uw_ft_grad_program_search_sort_numerically_alpha($a, $b)
Sorts an array by values using a user-defined comparison function. The algorithm determines how many hits there are with a program. We want it so that the programs that have the most number of hits are on the top of the list. This function sorts it by the number of hits a program has, followed by sorting it alphabetically.

##### Parameters
- $a: The first element to be compared.
- $b: The second element to be compared.

##### Returns
1 if we want $b to be sorted before $a, -1 if we want $b to be sorted after $a, and strnatcmp($a, $b) if we want them to be sorted naturally.

##### Additional Information
See the [api documentation](http://php.net/manual/en/function.usort.php) for usort for more information.
